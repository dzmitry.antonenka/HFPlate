//
// Created by Dmitry Antonenka on 4/2/17.
// Copyright (c) 2017 BSUIR. All rights reserved.
//

import UIKit

extension PlateStyleKit {
    @objc public dynamic class func generateStarLikeGranuleBezierPath(frame: CGRect) -> UIBezierPath {
        //// Bezier Drawing
        let starPath = UIBezierPath()
        starPath.move(to: CGPoint(x: frame.minX + 0.43750 * frame.width, y: frame.minY + 0.06667 * frame.height))
        starPath.addCurve(to: CGPoint(x: frame.minX + 0.74065 * frame.width, y: frame.minY + 0.17542 * frame.height), controlPoint1: CGPoint(x: frame.minX + 0.54220 * frame.width, y: frame.minY + 0.06667 * frame.height), controlPoint2: CGPoint(x: frame.minX + 0.74065 * frame.width, y: frame.minY + 0.17542 * frame.height))
        starPath.addCurve(to: CGPoint(x: frame.minX + 0.93750 * frame.width, y: frame.minY + 0.26667 * frame.height), controlPoint1: CGPoint(x: frame.minX + 0.74065 * frame.width, y: frame.minY + 0.17542 * frame.height), controlPoint2: CGPoint(x: frame.minX + 0.90515 * frame.width, y: frame.minY + 0.16604 * frame.height))
        starPath.addCurve(to: CGPoint(x: frame.minX + 0.75000 * frame.width, y: frame.minY + 0.60000 * frame.height), controlPoint1: CGPoint(x: frame.minX + 0.96985 * frame.width, y: frame.minY + 0.36729 * frame.height), controlPoint2: CGPoint(x: frame.minX + 0.80392 * frame.width, y: frame.minY + 0.43229 * frame.height))
        starPath.addCurve(to: CGPoint(x: frame.minX + 0.88025 * frame.width, y: frame.minY + 0.95208 * frame.height), controlPoint1: CGPoint(x: frame.minX + 0.69608 * frame.width, y: frame.minY + 0.76771 * frame.height), controlPoint2: CGPoint(x: frame.minX + 0.96495 * frame.width, y: frame.minY + 0.88989 * frame.height))
        starPath.addCurve(to: CGPoint(x: frame.minX + 0.53125 * frame.width, y: frame.minY + 0.82667 * frame.height), controlPoint1: CGPoint(x: frame.minX + 0.79554 * frame.width, y: frame.minY + 1.01427 * frame.height), controlPoint2: CGPoint(x: frame.minX + 0.53125 * frame.width, y: frame.minY + 0.82667 * frame.height))
        starPath.addCurve(to: CGPoint(x: frame.minX + 0.18225 * frame.width, y: frame.minY + 0.95208 * frame.height), controlPoint1: CGPoint(x: frame.minX + 0.53125 * frame.width, y: frame.minY + 0.82667 * frame.height), controlPoint2: CGPoint(x: frame.minX + 0.26696 * frame.width, y: frame.minY + 1.01427 * frame.height))
        starPath.addCurve(to: CGPoint(x: frame.minX + 0.19244 * frame.width, y: frame.minY + 0.57791 * frame.height), controlPoint1: CGPoint(x: frame.minX + 0.09755 * frame.width, y: frame.minY + 0.88989 * frame.height), controlPoint2: CGPoint(x: frame.minX + 0.19244 * frame.width, y: frame.minY + 0.57791 * frame.height))
        starPath.addCurve(to: CGPoint(x: frame.minX + 0.06250 * frame.width, y: frame.minY + 0.40000 * frame.height), controlPoint1: CGPoint(x: frame.minX + 0.19244 * frame.width, y: frame.minY + 0.57791 * frame.height), controlPoint2: CGPoint(x: frame.minX + 0.03015 * frame.width, y: frame.minY + 0.50062 * frame.height))
        starPath.addCurve(to: CGPoint(x: frame.minX + 0.31250 * frame.width, y: frame.minY + 0.26667 * frame.height), controlPoint1: CGPoint(x: frame.minX + 0.09485 * frame.width, y: frame.minY + 0.29938 * frame.height), controlPoint2: CGPoint(x: frame.minX + 0.31250 * frame.width, y: frame.minY + 0.26667 * frame.height))
        starPath.addCurve(to: CGPoint(x: frame.minX + 0.43750 * frame.width, y: frame.minY + 0.06667 * frame.height), controlPoint1: CGPoint(x: frame.minX + 0.31250 * frame.width, y: frame.minY + 0.26667 * frame.height), controlPoint2: CGPoint(x: frame.minX + 0.33280 * frame.width, y: frame.minY + 0.06667 * frame.height))
        starPath.close()

        return starPath
    }

    @objc public dynamic class func generateCircleGranuleBezierPath(frame: CGRect = CGRect(x: 0, y: 3, width: 13, height: 13), granulBorderWidth: CGFloat)-> UIBezierPath {
        
        //// Oval Drawing
        let ovalPath = UIBezierPath()
        ovalPath.move(to: CGPoint(x: frame.minX + 0.92857 * frame.width, y: frame.minY + 0.50000 * frame.height))
        ovalPath.addCurve(to: CGPoint(x: frame.minX + 0.50000 * frame.width, y: frame.minY + 0.92857 * frame.height), controlPoint1: CGPoint(x: frame.minX + 0.92857 * frame.width, y: frame.minY + 0.73669 * frame.height), controlPoint2: CGPoint(x: frame.minX + 0.73669 * frame.width, y: frame.minY + 0.92857 * frame.height))
        ovalPath.addCurve(to: CGPoint(x: frame.minX + 0.07143 * frame.width, y: frame.minY + 0.50000 * frame.height), controlPoint1: CGPoint(x: frame.minX + 0.26331 * frame.width, y: frame.minY + 0.92857 * frame.height), controlPoint2: CGPoint(x: frame.minX + 0.07143 * frame.width, y: frame.minY + 0.73669 * frame.height))
        ovalPath.addCurve(to: CGPoint(x: frame.minX + 0.50000 * frame.width, y: frame.minY + 0.07143 * frame.height), controlPoint1: CGPoint(x: frame.minX + 0.07143 * frame.width, y: frame.minY + 0.26331 * frame.height), controlPoint2: CGPoint(x: frame.minX + 0.26331 * frame.width, y: frame.minY + 0.07143 * frame.height))
        ovalPath.addCurve(to: CGPoint(x: frame.minX + 0.92857 * frame.width, y: frame.minY + 0.50000 * frame.height), controlPoint1: CGPoint(x: frame.minX + 0.73669 * frame.width, y: frame.minY + 0.07143 * frame.height), controlPoint2: CGPoint(x: frame.minX + 0.92857 * frame.width, y: frame.minY + 0.26331 * frame.height))
        ovalPath.close()
        
        return ovalPath
    }
    
    @objc public dynamic class func generateGranularPlateBottomCurveBezierPath(plateFrame: CGRect = CGRect(x: 0, y: 0, width: 250, height: 173), edgeWidth: CGFloat = 2) -> UIBezierPath {
        //// General Declarations
        // This non-generic function dramatically improves compilation times of complex expressions.
        func fastFloor(_ x: CGFloat) -> CGFloat { return floor(x) }
        
        //// Subframes
        let plate: CGRect = CGRect(x: plateFrame.minX + fastFloor(plateFrame.width * -0.13400) + 0.5, y: plateFrame.minY + fastFloor(plateFrame.height * 0.00417) + 0.5, width: fastFloor(plateFrame.width * 1.18858 + 0.36) - fastFloor(plateFrame.width * -0.13400) - 0.36, height: fastFloor(plateFrame.height * 1.70371 + 0.05) - fastFloor(plateFrame.height * 0.00417) - 0.05)
        
        //// Plate
        //// Fill Border Drawing
        let fillBorderPath = UIBezierPath()
        fillBorderPath.move(to: CGPoint(x: plate.minX + 0.00000 * plate.width, y: plate.minY + 0.00000 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.00000 * plate.width, y: plate.minY + 0.10297 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.00000 * plate.width, y: plate.minY + 0.46091 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.00000 * plate.width, y: plate.minY + 0.57859 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.00000 * plate.width, y: plate.minY + 0.98065 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.34781 * plate.width, y: plate.minY + 0.98113 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.34781 * plate.width, y: plate.minY + 0.57935 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.32966 * plate.width, y: plate.minY + 0.54587 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.27522 * plate.width, y: plate.minY + 0.52196 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.22078 * plate.width, y: plate.minY + 0.46091 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.19961 * plate.width, y: plate.minY + 0.42168 * plate.height))
        fillBorderPath.addCurve(to: CGPoint(x: plate.minX + 0.18030 * plate.width, y: plate.minY + 0.34597 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.19961 * plate.width, y: plate.minY + 0.42168 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.18257 * plate.width, y: plate.minY + 0.35945 * plate.height))
        fillBorderPath.addCurve(to: CGPoint(x: plate.minX + 0.18449 * plate.width, y: plate.minY + 0.34323 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.17913 * plate.width, y: plate.minY + 0.33899 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.18125 * plate.width, y: plate.minY + 0.33272 * plate.height))
        fillBorderPath.addCurve(to: CGPoint(x: plate.minX + 0.19054 * plate.width, y: plate.minY + 0.36775 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.18751 * plate.width, y: plate.minY + 0.35304 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.19054 * plate.width, y: plate.minY + 0.36775 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.19659 * plate.width, y: plate.minY + 0.39226 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.20566 * plate.width, y: plate.minY + 0.41678 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.22185 * plate.width, y: plate.minY + 0.45022 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.24384 * plate.width, y: plate.minY + 0.47929 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.26583 * plate.width, y: plate.minY + 0.50067 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.29760 * plate.width, y: plate.minY + 0.52229 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.32448 * plate.width, y: plate.minY + 0.53368 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.39291 * plate.width, y: plate.minY + 0.54522 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.48577 * plate.width, y: plate.minY + 0.54906 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.55419 * plate.width, y: plate.minY + 0.54906 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.63239 * plate.width, y: plate.minY + 0.53670 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.66171 * plate.width, y: plate.minY + 0.52599 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.69027 * plate.width, y: plate.minY + 0.50763 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.71490 * plate.width, y: plate.minY + 0.48626 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.73461 * plate.width, y: plate.minY + 0.46464 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.75432 * plate.width, y: plate.minY + 0.43581 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.77156 * plate.width, y: plate.minY + 0.39977 * plate.height))
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.78390 * plate.width, y: plate.minY + 0.34933 * plate.height))
        fillBorderPath.addCurve(to: CGPoint(x: plate.minX + 0.78332 * plate.width, y: plate.minY + 0.34498 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.78390 * plate.width, y: plate.minY + 0.34933 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.78119 * plate.width, y: plate.minY + 0.34966 * plate.height))
        fillBorderPath.addCurve(to: CGPoint(x: plate.minX + 0.78937 * plate.width, y: plate.minY + 0.34977 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.78544 * plate.width, y: plate.minY + 0.34031 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.78937 * plate.width, y: plate.minY + 0.33900 * plate.height))
        fillBorderPath.addCurve(to: CGPoint(x: plate.minX + 0.77122 * plate.width, y: plate.minY + 0.41195 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.78937 * plate.width, y: plate.minY + 0.35335 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.78119 * plate.width, y: plate.minY + 0.37720 * plate.height))
        fillBorderPath.addCurve(to: CGPoint(x: plate.minX + 0.75307 * plate.width, y: plate.minY + 0.45021 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.76948 * plate.width, y: plate.minY + 0.41802 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.75484 * plate.width, y: plate.minY + 0.44389 * plate.height))
        fillBorderPath.addCurve(to: CGPoint(x: plate.minX + 0.74703 * plate.width, y: plate.minY + 0.45978 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.75157 * plate.width, y: plate.minY + 0.45558 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.75262 * plate.width, y: plate.minY + 0.45123 * plate.height))
        fillBorderPath.addCurve(to: CGPoint(x: plate.minX + 0.71376 * plate.width, y: plate.minY + 0.50282 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.74329 * plate.width, y: plate.minY + 0.46549 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.73033 * plate.width, y: plate.minY + 0.48535 * plate.height))
        fillBorderPath.addCurve(to: CGPoint(x: plate.minX + 0.68049 * plate.width, y: plate.minY + 0.52674 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.70468 * plate.width, y: plate.minY + 0.51239 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.68956 * plate.width, y: plate.minY + 0.52196 * plate.height))
        fillBorderPath.addCurve(to: CGPoint(x: plate.minX + 0.63210 * plate.width, y: plate.minY + 0.55066 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.65928 * plate.width, y: plate.minY + 0.53792 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.63345 * plate.width, y: plate.minY + 0.54602 * plate.height))
        fillBorderPath.addCurve(to: CGPoint(x: plate.minX + 0.63210 * plate.width, y: plate.minY + 0.56022 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.63040 * plate.width, y: plate.minY + 0.55647 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.63308 * plate.width, y: plate.minY + 0.55340 * plate.height))
        fillBorderPath.addCurve(to: CGPoint(x: plate.minX + 0.63210 * plate.width, y: plate.minY + 0.98113 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.62780 * plate.width, y: plate.minY + 0.58994 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.63080 * plate.width, y: plate.minY + 0.92116 * plate.height))
        fillBorderPath.addCurve(to: CGPoint(x: plate.minX + 0.99805 * plate.width, y: plate.minY + 0.98065 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.63290 * plate.width, y: plate.minY + 1.01811 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.94030 * plate.width, y: plate.minY + 0.99115 * plate.height))
        fillBorderPath.addCurve(to: CGPoint(x: plate.minX + 0.99805 * plate.width, y: plate.minY + 0.57859 * plate.height), controlPoint1: CGPoint(x: plate.minX + 1.00244 * plate.width, y: plate.minY + 0.97986 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.99805 * plate.width, y: plate.minY + 0.66660 * plate.height))
        fillBorderPath.addCurve(to: CGPoint(x: plate.minX + 0.99805 * plate.width, y: plate.minY + 0.00000 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.99805 * plate.width, y: plate.minY + 0.36813 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.99805 * plate.width, y: plate.minY + 0.00000 * plate.height))
        fillBorderPath.lineWidth = 1

        return fillBorderPath
    }
}

