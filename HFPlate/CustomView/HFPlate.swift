//
// Created by Dmitry Antonenka on 4/2/17.
// Copyright (c) 2017 BSUIR. All rights reserved.
//

import UIKit

/// @Abstract class
open class HFPlate: UIView {
    @IBInspectable open var animationTime: CFTimeInterval = 0.4

    
    @IBInspectable open var minValue: CGFloat = 100 {
        didSet {
            setNeedsDisplay()
        }
    }
    @IBInspectable open var maxValue: CGFloat = 500 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    ///Help variable to hold current fill progress
    internal var _previousProgressPercentage: CGFloat = 0.0
    internal var _currentProgressPercentage: CGFloat = 0.0
    internal var _isLoaded: Bool = false
    
    @IBInspectable open var currentValue: CGFloat = 0.0 {
        didSet {
            guard _isLoaded, maxValue > 0
            else {
                return
            }
            
            _currentProgressPercentage = currentValue / maxValue
            animateFill(from: _previousProgressPercentage, to: _currentProgressPercentage)
        }
    }
    
    
    //MARK: Supplementary views.
    
    @IBInspectable open var edgeColor: UIColor = UIColor.white {
        didSet {
            setNeedsDisplay()
        }
    }
    @IBInspectable open var edgeWidth: CGFloat = 3.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    @IBInspectable open var topElipseEdgeWidth: CGFloat = 1.0 {
        didSet {
            setNeedsDisplay()
        }
    }

    //MARK: Interface

    open override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor.clear
    }

    override open func draw(_ rect: CGRect) {
        assert(false, "draw(_:) needs to be overridden!")
    }

    //MARK: Interface to be overridden

    internal func animateFill(from oldProgress: CGFloat, to newProgress: CGFloat) {
        assert(false, "Method should be overridden !")
    }
}


