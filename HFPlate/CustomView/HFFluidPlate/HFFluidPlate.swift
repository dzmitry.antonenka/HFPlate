//
// Created by Dmitry Antonenka on 4/3/17.
// Copyright (c) 2017 BSUIR. All rights reserved.
//

import UIKit

@IBDesignable
open class HFFluidPlate: HFPlate {

    //--------------------------------------------------------
    //MARK: @IBInspectable properties

    @IBInspectable open var plateFillMultiplier: CGFloat = 0.6 {
        didSet {
            setNeedsDisplay()
        }
    }

    @IBInspectable open var fillLightColor: UIColor = UIColor.green.withAlphaComponent(0.4) {
        didSet {
            setNeedsDisplay()
        }
    }

    @IBInspectable open var fillDarkColor: UIColor = UIColor.green {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable override open var currentValue: CGFloat {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable override open var topElipseEdgeWidth: CGFloat {
        didSet {
            setNeedsDisplay()
        }
    }

    //--------------------------------------------------------
    //MARK: Fileprivate properties

    fileprivate var glassTopEllipseYCenter: CGFloat = 0.0

    //Represents inner curve. We need this property to calculate max Y groove.
    fileprivate var innerCurveYBottom: CGFloat = 0.0

    fileprivate var glassTopEllipseXStart: CGFloat = 0.0
    fileprivate var glassTopEllipseXEnd: CGFloat = 0.0

    //Array of points that used to fill plate curve using `broken` lines. 
    //These pints are these lines coordinates.
    fileprivate var innerGrooveFillPointsQueue = [CGPoint]()
    fileprivate var fillGroovePointsQueue = [CGPoint]()

    
    fileprivate var overlayEllipseHeight: CGFloat = 0.0
    fileprivate var currentOverlayXStart: CGFloat = 0.0
    fileprivate var currentOverlayXEnd: CGFloat = 0.0
    fileprivate var currentOverlayYValue: CGFloat = 0.0
    
    //Array that hold previous and current rendered CALayers.
    //Created to improve animation
    fileprivate var fillCAShapeLayerArrayNew = [CAShapeLayer]()
    fileprivate var fillCAShapeLayerArrayOld = [CAShapeLayer]()
    
    fileprivate var _plateFluidPartView: UIView!
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        
        allocAndBindPlateFluidPartView()
        
        _isLoaded = true
    }
    
    func allocAndBindPlateFluidPartView()  {
        guard let superview = self.superview else {
            print("Unable to get superview")
            return
        }
        
        _plateFluidPartView = UIView()
        _plateFluidPartView.translatesAutoresizingMaskIntoConstraints = false
        
        superview.insertSubview(_plateFluidPartView, belowSubview: self)
        
        NSLayoutConstraint(item: _plateFluidPartView as Any, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: _plateFluidPartView as Any, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: _plateFluidPartView as Any, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: _plateFluidPartView as Any, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0.0).isActive = true
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    //--------------------------------------------------------
    //MARK: Public interface
    // *PaintCode StyleKit Copy-paste to create references to desired points/elements*

    override open func draw(_ rect: CGRect) {
        backgroundColor = UIColor.clear

        //Copy new items to old array.
        fillCAShapeLayerArrayOld.append(contentsOf: fillCAShapeLayerArrayNew)
        fillCAShapeLayerArrayNew.removeAll()
        
        
        drawFluidPlate(plateFrame: rect, edgeWidth: edgeWidth, edgeColor: self.edgeColor)
        
        drawPreviousState()
        
        removeOldSublayers()
        
        //Filter points in inner curve to be able to draw fill animation.
        filterCurvePointsInQueue()
        
        animateFill(from: _previousProgressPercentage, to: _currentProgressPercentage)
    }

    override open func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()

        UIGraphicsBeginImageContext(self.frame.size)
        
        backgroundColor = UIColor.clear

//Need superview :(
//        allocAndBindPlateFluidPartView()
        _isLoaded = true
        
        draw(self.frame)
        
        UIGraphicsEndImageContext();
    }


    //--------------------------------------------------------
    //MARK: Fileprivate methods

    
    fileprivate func drawPreviousState() {
        for layer in self.fillCAShapeLayerArrayOld.reversed() {
            self.layer.addSublayer(layer)
        }
    }
    
    fileprivate func removeOldSublayers() {
        let disappearAnimationTime: CFTimeInterval = animationTime/2
        let progressGreaterThanZero = (_currentProgressPercentage - _previousProgressPercentage > 0)
        
        for (index, layer) in self.fillCAShapeLayerArrayOld.enumerated() {
            var disappearAnimation: CABasicAnimation = self.getFillAnimation(with: disappearAnimationTime, fromColor: layer.fillColor!, toColor: UIColor.clear.cgColor)
            
            
            if index < self.fillCAShapeLayerArrayOld.count - 1, !progressGreaterThanZero {
                let layerStartAnimationColor = UIColor.init(cgColor: layer.fillColor!).withAlphaComponent(0.2).cgColor
                
                disappearAnimation = self.getFillAnimation(with: disappearAnimationTime/2,
                                                          fromColor: layerStartAnimationColor, toColor: UIColor.clear.cgColor)
            }
            
            layer.add( disappearAnimation, forKey: "fillColor")
            
            //Set fill color to clear disabling all transactions.
            CATransaction.begin()
            CATransaction.setDisableActions(true)
            layer.fillColor = UIColor.clear.cgColor
            CATransaction.commit()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + disappearAnimationTime ) {
            for layer in self.fillCAShapeLayerArrayOld {
                layer.removeAllAnimations()
                layer.removeFromSuperlayer()
            }
            
            self.fillCAShapeLayerArrayOld.removeAll()
        }
    }
    
    ///Fill `fillGroovePointsQueue` with points to be able calculate intermediate points in half of the curve.
    fileprivate func filterCurvePointsInQueue() {
        guard let topStartPoint = innerGrooveFillPointsQueue.first else {
            return
        }
        
        var currentPoint: CGPoint = topStartPoint
        fillGroovePointsQueue.removeAll()
        
        let itemsCount = innerGrooveFillPointsQueue.count
        for itemIndex in 1..<itemsCount {
            let nextPoint = innerGrooveFillPointsQueue[itemIndex]
         
            if nextPoint.y >= currentPoint.y {
                currentPoint = nextPoint 
            } else {
                break
            }

            fillGroovePointsQueue.append(nextPoint)
        }
    }
    
    
    ///Generate path from multiple discrete lines to emulate curve.
    fileprivate func generateBezierPathForBorderCurve(plateRect plate: CGRect) -> UIBezierPath {
        //TODO: find method to iterate through UIBezierPath points. For now extension for CGPath: points() method throws exception.
        //May be some interpolation

        //// Plate
        //// Fill Border Drawing
        let fillBorderPath = UIBezierPath()
        fillBorderPath.move(to: CGPoint(x: plate.minX + 0.00606 * plate.width, y: plate.minY + 0.10905 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.01009 * plate.width, y: plate.minY + 0.17535 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.01262 * plate.width, y: plate.minY + 0.19605 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.01469 * plate.width, y: plate.minY + 0.21298 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.01643 * plate.width, y: plate.minY + 0.22720 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.01817 * plate.width, y: plate.minY + 0.24141 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.02059 * plate.width, y: plate.minY + 0.25659 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.02314 * plate.width, y: plate.minY + 0.27251 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.02534 * plate.width, y: plate.minY + 0.28632 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.02790 * plate.width, y: plate.minY + 0.30234 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.03032 * plate.width, y: plate.minY + 0.31743 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.03332 * plate.width, y: plate.minY + 0.33512 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.03572 * plate.width, y: plate.minY + 0.34921 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.03851 * plate.width, y: plate.minY + 0.36563 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.04126 * plate.width, y: plate.minY + 0.38182 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.04397 * plate.width, y: plate.minY + 0.39773 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.04651 * plate.width, y: plate.minY + 0.41269 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.05022 * plate.width, y: plate.minY + 0.42837 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.05398 * plate.width, y: plate.minY + 0.44423 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.05720 * plate.width, y: plate.minY + 0.45785 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.06082 * plate.width, y: plate.minY + 0.47316 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.06378 * plate.width, y: plate.minY + 0.48568 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.06675 * plate.width, y: plate.minY + 0.49822 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.07320 * plate.width, y: plate.minY + 0.51879 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.07766 * plate.width, y: plate.minY + 0.53303 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.08242 * plate.width, y: plate.minY + 0.54821 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.08700 * plate.width, y: plate.minY + 0.56280 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.09088 * plate.width, y: plate.minY + 0.57460 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.09527 * plate.width, y: plate.minY + 0.58794 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.09939 * plate.width, y: plate.minY + 0.60048 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.10319 * plate.width, y: plate.minY + 0.61202 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.10701 * plate.width, y: plate.minY + 0.62233 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.11137 * plate.width, y: plate.minY + 0.63410 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.11519 * plate.width, y: plate.minY + 0.64441 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.11995 * plate.width, y: plate.minY + 0.65726 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.12343 * plate.width, y: plate.minY + 0.66667 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.12732 * plate.width, y: plate.minY + 0.67417 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.13168 * plate.width, y: plate.minY + 0.68257 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.13786 * plate.width, y: plate.minY + 0.69449 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.14482 * plate.width, y: plate.minY + 0.70791 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.15177 * plate.width, y: plate.minY + 0.72131 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.15759 * plate.width, y: plate.minY + 0.73171 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.16343 * plate.width, y: plate.minY + 0.74214 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.16953 * plate.width, y: plate.minY + 0.75304 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.17610 * plate.width, y: plate.minY + 0.76478 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.18262 * plate.width, y: plate.minY + 0.77227 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.19018 * plate.width, y: plate.minY + 0.78094 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.19761 * plate.width, y: plate.minY + 0.78947 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.20445 * plate.width, y: plate.minY + 0.79733 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.21240 * plate.width, y: plate.minY + 0.80659 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.21873 * plate.width, y: plate.minY + 0.81398 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.22541 * plate.width, y: plate.minY + 0.82176 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.23278 * plate.width, y: plate.minY + 0.83036 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.24030 * plate.width, y: plate.minY + 0.83543 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.24886 * plate.width, y: plate.minY + 0.84121 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.25649 * plate.width, y: plate.minY + 0.84636 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.26517 * plate.width, y: plate.minY + 0.85221 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.27392 * plate.width, y: plate.minY + 0.85616 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.28117 * plate.width, y: plate.minY + 0.85942 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.28946 * plate.width, y: plate.minY + 0.86314 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.29678 * plate.width, y: plate.minY + 0.86644 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.30486 * plate.width, y: plate.minY + 0.87007 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.31375 * plate.width, y: plate.minY + 0.87407 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.32415 * plate.width, y: plate.minY + 0.87808 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.33238 * plate.width, y: plate.minY + 0.88125 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.34211 * plate.width, y: plate.minY + 0.88500 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.35648 * plate.width, y: plate.minY + 0.88799 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.36460 * plate.width, y: plate.minY + 0.88967 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.37408 * plate.width, y: plate.minY + 0.89164 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.38238 * plate.width, y: plate.minY + 0.89337 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.39470 * plate.width, y: plate.minY + 0.89593 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.40334 * plate.width, y: plate.minY + 0.89772 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.41492 * plate.width, y: plate.minY + 0.90013 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.42561 * plate.width, y: plate.minY + 0.90234 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.43534 * plate.width, y: plate.minY + 0.90436 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.44737 * plate.width, y: plate.minY + 0.90686 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.46189 * plate.width, y: plate.minY + 0.90686 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.47048 * plate.width, y: plate.minY + 0.90686 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.47948 * plate.width, y: plate.minY + 0.90686 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.48841 * plate.width, y: plate.minY + 0.90686 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.49595 * plate.width, y: plate.minY + 0.90686 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.50380 * plate.width, y: plate.minY + 0.90686 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.51209 * plate.width, y: plate.minY + 0.90686 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.51745 * plate.width, y: plate.minY + 0.90686 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.52588 * plate.width, y: plate.minY + 0.90686 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.53237 * plate.width, y: plate.minY + 0.90686 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.53857 * plate.width, y: plate.minY + 0.90579 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.54411 * plate.width, y: plate.minY + 0.90483 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.55081 * plate.width, y: plate.minY + 0.90367 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.55989 * plate.width, y: plate.minY + 0.90211 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.56840 * plate.width, y: plate.minY + 0.90063 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.57627 * plate.width, y: plate.minY + 0.89927 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.58315 * plate.width, y: plate.minY + 0.89809 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.58980 * plate.width, y: plate.minY + 0.89694 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.59704 * plate.width, y: plate.minY + 0.89569 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.60538 * plate.width, y: plate.minY + 0.89319 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.61435 * plate.width, y: plate.minY + 0.89051 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.62178 * plate.width, y: plate.minY + 0.88829 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.62771 * plate.width, y: plate.minY + 0.88652 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.63389 * plate.width, y: plate.minY + 0.88467 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.64267 * plate.width, y: plate.minY + 0.88230 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.65141 * plate.width, y: plate.minY + 0.87994 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.65784 * plate.width, y: plate.minY + 0.87820 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.66308 * plate.width, y: plate.minY + 0.87678 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.66827 * plate.width, y: plate.minY + 0.87538 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.67402 * plate.width, y: plate.minY + 0.87383 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.68095 * plate.width, y: plate.minY + 0.87009 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.68677 * plate.width, y: plate.minY + 0.86696 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.69388 * plate.width, y: plate.minY + 0.86313 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.70154 * plate.width, y: plate.minY + 0.85899 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.70834 * plate.width, y: plate.minY + 0.85533 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.71457 * plate.width, y: plate.minY + 0.85197 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.72217 * plate.width, y: plate.minY + 0.84684 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.72938 * plate.width, y: plate.minY + 0.84198 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.73507 * plate.width, y: plate.minY + 0.83813 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.74103 * plate.width, y: plate.minY + 0.83411 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.74695 * plate.width, y: plate.minY + 0.83011 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.75172 * plate.width, y: plate.minY + 0.82643 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.75767 * plate.width, y: plate.minY + 0.82184 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.76273 * plate.width, y: plate.minY + 0.81793 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.76895 * plate.width, y: plate.minY + 0.81313 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.77527 * plate.width, y: plate.minY + 0.80825 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.78043 * plate.width, y: plate.minY + 0.80361 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.78518 * plate.width, y: plate.minY + 0.79934 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.79125 * plate.width, y: plate.minY + 0.79388 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.79956 * plate.width, y: plate.minY + 0.78640 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.80576 * plate.width, y: plate.minY + 0.77804 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.81178 * plate.width, y: plate.minY + 0.76990 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.81731 * plate.width, y: plate.minY + 0.76244 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.82385 * plate.width, y: plate.minY + 0.75361 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.82943 * plate.width, y: plate.minY + 0.74286 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.83621 * plate.width, y: plate.minY + 0.72980 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.84166 * plate.width, y: plate.minY + 0.71930 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.84713 * plate.width, y: plate.minY + 0.70875 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.85221 * plate.width, y: plate.minY + 0.69897 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.85744 * plate.width, y: plate.minY + 0.68682 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.86368 * plate.width, y: plate.minY + 0.67234 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.86913 * plate.width, y: plate.minY + 0.65968 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.87527 * plate.width, y: plate.minY + 0.64542 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.88055 * plate.width, y: plate.minY + 0.63315 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.88531 * plate.width, y: plate.minY + 0.62024 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.88880 * plate.width, y: plate.minY + 0.61077 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.89305 * plate.width, y: plate.minY + 0.59926 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.89729 * plate.width, y: plate.minY + 0.58777 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.90084 * plate.width, y: plate.minY + 0.57812 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.90483 * plate.width, y: plate.minY + 0.56733 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.90835 * plate.width, y: plate.minY + 0.55632 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.91201 * plate.width, y: plate.minY + 0.54488 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.91634 * plate.width, y: plate.minY + 0.53134 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.91988 * plate.width, y: plate.minY + 0.52028 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.92332 * plate.width, y: plate.minY + 0.50952 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.92631 * plate.width, y: plate.minY + 0.50019 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.92915 * plate.width, y: plate.minY + 0.49131 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.93296 * plate.width, y: plate.minY + 0.47700 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.93577 * plate.width, y: plate.minY + 0.46649 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.93789 * plate.width, y: plate.minY + 0.45854 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.94103 * plate.width, y: plate.minY + 0.44674 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.94375 * plate.width, y: plate.minY + 0.43653 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.94636 * plate.width, y: plate.minY + 0.42673 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.94941 * plate.width, y: plate.minY + 0.41530 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.95164 * plate.width, y: plate.minY + 0.40353 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.95471 * plate.width, y: plate.minY + 0.38727 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.95754 * plate.width, y: plate.minY + 0.37232 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.96010 * plate.width, y: plate.minY + 0.35984 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.96318 * plate.width, y: plate.minY + 0.34484 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.96565 * plate.width, y: plate.minY + 0.33285 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.96763 * plate.width, y: plate.minY + 0.32320 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.96976 * plate.width, y: plate.minY + 0.31285 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.97175 * plate.width, y: plate.minY + 0.29906 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.97388 * plate.width, y: plate.minY + 0.28428 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.97575 * plate.width, y: plate.minY + 0.27136 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.97782 * plate.width, y: plate.minY + 0.25698 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.97964 * plate.width, y: plate.minY + 0.24350 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.98163 * plate.width, y: plate.minY + 0.22869 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.98334 * plate.width, y: plate.minY + 0.21596 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.98586 * plate.width, y: plate.minY + 0.19721 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.98693 * plate.width, y: plate.minY + 0.18704 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.98830 * plate.width, y: plate.minY + 0.17387 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.98931 * plate.width, y: plate.minY + 0.16424 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.99068 * plate.width, y: plate.minY + 0.15118 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        fillBorderPath.addLine(to: CGPoint(x: plate.minX + 0.99394 * plate.width, y: plate.minY + 0.11997 * plate.height))
        innerGrooveFillPointsQueue.append(fillBorderPath.currentPoint)
        
        return fillBorderPath
    }

    fileprivate final func drawFluidPlate(plateFrame: CGRect = CGRect(x: 0, y: 0, width: 250, height: 120), edgeWidth: CGFloat = 2, edgeColor: UIColor = UIColor(red: 1.000, green: 1.000, blue: 1.000, alpha: 1.000)) {
        //// General Declarations
        // This non-generic function dramatically improves compilation times of complex expressions.
        func fastFloor(_ x: CGFloat) -> CGFloat { return floor(x) }
        
        //// Subframes
        let plate: CGRect = CGRect(x: plateFrame.minX + fastFloor(plateFrame.width * 0.00800 + 0.5), y: plateFrame.minY + fastFloor(plateFrame.height * 0.03684) + 0.5, width: fastFloor(plateFrame.width * 1.00000 + 0.5) - fastFloor(plateFrame.width * 0.00800 + 0.5), height: fastFloor(plateFrame.height * 0.95789 + 0.5) - fastFloor(plateFrame.height * 0.03684) - 0.5)
        
        
        //// Plate
        //// Fill Border Drawing
        let _ = generateBezierPathForBorderCurve(plateRect: plate)

        //// Plate Main Part
        //// Plate Bottom Curve Drawing
        let plateBottomCurvePath = UIBezierPath()
        plateBottomCurvePath.move(to: CGPoint(x: plate.minX + 0.00000 * plate.width, y: plate.minY + 0.09271 * plate.height))
        plateBottomCurvePath.addCurve(to: CGPoint(x: plate.minX + 0.23744 * plate.width, y: plate.minY + 0.84442 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.04582 * plate.width, y: plate.minY + 0.72676 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.23744 * plate.width, y: plate.minY + 0.84442 * plate.height))
        plateBottomCurvePath.addCurve(to: CGPoint(x: plate.minX + 0.50403 * plate.width, y: plate.minY + 0.91620 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.23744 * plate.width, y: plate.minY + 0.84442 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.33012 * plate.width, y: plate.minY + 0.91620 * plate.height))
        plateBottomCurvePath.move(to: CGPoint(x: plate.minX + 1.00000 * plate.width, y: plate.minY + 0.09271 * plate.height))
        plateBottomCurvePath.addCurve(to: CGPoint(x: plate.minX + 0.88354 * plate.width, y: plate.minY + 0.64789 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.98075 * plate.width, y: plate.minY + 0.35902 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.92840 * plate.width, y: plate.minY + 0.53423 * plate.height))
        plateBottomCurvePath.addCurve(to: CGPoint(x: plate.minX + 0.76660 * plate.width, y: plate.minY + 0.83316 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.82160 * plate.width, y: plate.minY + 0.80484 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.76660 * plate.width, y: plate.minY + 0.83316 * plate.height))
        plateBottomCurvePath.addCurve(to: CGPoint(x: plate.minX + 0.50000 * plate.width, y: plate.minY + 0.91620 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.76660 * plate.width, y: plate.minY + 0.83316 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.67391 * plate.width, y: plate.minY + 0.91620 * plate.height))
        edgeColor.setStroke()
        plateBottomCurvePath.lineWidth = edgeWidth
        plateBottomCurvePath.stroke()

        
        //// Plate Top Ellipse Drawing
        let topEllipseRect = CGRect(x: plate.minX + fastFloor(plate.width * 0.00000 + 0.5), y: plate.minY + fastFloor(plate.height * 0.00000 + 0.5), width: fastFloor(plate.width * 1.00000 + 0.5) - fastFloor(plate.width * 0.00000 + 0.5), height: fastFloor(plate.height * 0.21564 + 0.2) - fastFloor(plate.height * 0.00000 + 0.5) + 0.3)
        let plateTopEllipsePath = UIBezierPath(ovalIn: topEllipseRect)
        edgeColor.setStroke()
        plateTopEllipsePath.lineWidth = topElipseEdgeWidth
        plateTopEllipsePath.stroke()
        
        glassTopEllipseYCenter = topEllipseRect.midY
        glassTopEllipseXStart = topEllipseRect.minX
        glassTopEllipseXEnd = topEllipseRect.maxX

        //Find max depth for plate inner curve.
        var currentPoint = innerGrooveFillPointsQueue.first!
        for point in innerGrooveFillPointsQueue {
              if point.y > currentPoint.y {
                  innerCurveYBottom = point.y
                  currentPoint = point
              }
        }


        overlayEllipseHeight = topEllipseRect.height

        //// stand bezier path Drawing
        let standBezierPathPath = UIBezierPath()
        standBezierPathPath.move(to: CGPoint(x: plate.minX + 0.25403 * plate.width, y: plate.minY + 0.87429 * plate.height))
        standBezierPathPath.addCurve(to: CGPoint(x: plate.minX + 0.25575 * plate.width, y: plate.minY + 0.89536 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.25403 * plate.width, y: plate.minY + 0.88164 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.25466 * plate.width, y: plate.minY + 0.88868 * plate.height))
        standBezierPathPath.addCurve(to: CGPoint(x: plate.minX + 0.29034 * plate.width, y: plate.minY + 0.96797 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.26294 * plate.width, y: plate.minY + 0.93957 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.29034 * plate.width, y: plate.minY + 0.96797 * plate.height))
        standBezierPathPath.addCurve(to: CGPoint(x: plate.minX + 0.35548 * plate.width, y: plate.minY + 0.98799 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.29034 * plate.width, y: plate.minY + 0.96797 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.30052 * plate.width, y: plate.minY + 0.97898 * plate.height))
        standBezierPathPath.addCurve(to: CGPoint(x: plate.minX + 0.49662 * plate.width, y: plate.minY + 1.00000 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.41044 * plate.width, y: plate.minY + 0.99700 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.47491 * plate.width, y: plate.minY + 1.00000 * plate.height))
        standBezierPathPath.move(to: CGPoint(x: plate.minX + 0.73790 * plate.width, y: plate.minY + 0.87429 * plate.height))
        standBezierPathPath.addCurve(to: CGPoint(x: plate.minX + 0.70289 * plate.width, y: plate.minY + 0.96797 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.73790 * plate.width, y: plate.minY + 0.93033 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.70289 * plate.width, y: plate.minY + 0.96797 * plate.height))
        standBezierPathPath.addCurve(to: CGPoint(x: plate.minX + 0.63775 * plate.width, y: plate.minY + 0.98799 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.70289 * plate.width, y: plate.minY + 0.96797 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.69271 * plate.width, y: plate.minY + 0.97898 * plate.height))
        standBezierPathPath.addCurve(to: CGPoint(x: plate.minX + 0.49662 * plate.width, y: plate.minY + 1.00000 * plate.height), controlPoint1: CGPoint(x: plate.minX + 0.58279 * plate.width, y: plate.minY + 0.99700 * plate.height), controlPoint2: CGPoint(x: plate.minX + 0.51833 * plate.width, y: plate.minY + 1.00000 * plate.height))
        edgeColor.setStroke()
        standBezierPathPath.lineWidth = edgeWidth
        standBezierPathPath.stroke()
    }

    fileprivate final func getInnerCurvePointsPair(between searchPositionY: CGFloat) -> (CGPoint, CGPoint) {
        var startEndPointPair: (CGPoint, CGPoint) = (.zero, .zero)
        let reversedArray = fillGroovePointsQueue

        for iterationPoint in reversedArray {
            if iterationPoint.y < searchPositionY {
                startEndPointPair.0 = iterationPoint
            }
            if iterationPoint.y > searchPositionY {
                startEndPointPair.1 = iterationPoint
            }
        }
        
        return startEndPointPair
    }

    fileprivate func calculatePointsForHelpOverlayPolygon(destinationY: CGFloat,
                                                          fillProgress: CGFloat,
                                                          pair: (startP: CGPoint, endP: CGPoint)) -> [CGPoint] {
        ///We need to add overlay to be sure all plate is filled.

        //Array that contains left side(curve) of path.
        var overlayIntermediatePointsForLeftSide = [CGPoint]()

        //Array that contains right side(curve) of path.
        var overlayIntermediatePointsForRightSide = [CGPoint]()

        //For now we need reversed array to go from middle of the plate to left border
        let reversedFillGroovePoints = fillGroovePointsQueue.reversed()
        let plateMiddleXPoint = abs(glassTopEllipseXStart - glassTopEllipseXEnd)/2
        
        //FIXME: For now plate is NOT a symmetrical to the middle.
        // So we introduce @deviationValue: CGFloat. calculated experimentally.
        let deviationValue = 4 * fillProgress
        
        for point in reversedFillGroovePoints {
            let distanceToMiddleXPoint = abs(plateMiddleXPoint - point.x)
            let symmetricPoint = CGPoint(x: point.x + distanceToMiddleXPoint * 2 + deviationValue, y: point.y)

            //Append point to left part of path
            overlayIntermediatePointsForLeftSide.append(point)

            //Append symmetricPoint to right part of path
            overlayIntermediatePointsForRightSide.append(symmetricPoint)

            //End iterating. All intermediate path points are copied to array for left side of path
            if point == pair.0 {
                currentOverlayXStart = point.x
                currentOverlayXEnd = symmetricPoint.x
                currentOverlayYValue = point.y
                
                break
            }
        }

        //FIXME: For now we don't include destination point to path because of very small error.
        //Error is very small because the number of discrete points big enough to emulate curve
        overlayIntermediatePointsForLeftSide.append(contentsOf: overlayIntermediatePointsForRightSide.reversed())

        //result array if merge of left side part and reversed right side.
        let resultPointsArray = overlayIntermediatePointsForLeftSide
        return resultPointsArray
    }

    override func animateFill(from oldProgress: CGFloat, to newProgress: CGFloat) {
        guard newProgress > 0 else { return } // Empty plate displayed

        let plateGrooveHeight = abs(glassTopEllipseYCenter - innerCurveYBottom)
        let deltaY = plateGrooveHeight * plateFillMultiplier

        let destinationYPoint = innerCurveYBottom - deltaY * (newProgress)

        ///----------------------------------------------
        ///CREATE PROPERTIES FOR `HELP` OVERLAY SHAPE LAYER

        //Get inner curve bezier path pair of points
        //where destination point by axis Y is between them
        let startEndPointPair = getInnerCurvePointsPair(between: destinationYPoint)
        let overlayPoints = calculatePointsForHelpOverlayPolygon(destinationY: destinationYPoint, fillProgress: newProgress, pair: startEndPointPair)

        guard overlayPoints.count > 0 else { return }

        let polylinePath = UIBezierPath()
        polylinePath.move(to: overlayPoints.first!)
        for item in overlayPoints {
            if item == overlayPoints.first {
                continue
            }

            polylinePath.addLine(to: item)
        }

        let helpOverlayLayer = getShapeLayer(for: polylinePath.cgPath, with: fillLightColor)
        _plateFluidPartView.layer.addSublayer(helpOverlayLayer)
  
        fillCAShapeLayerArrayNew.append(helpOverlayLayer)
        
        ///----------------------------------------------
        ///CREATE PROPERTIES FOR TOP OVERLAY ELLIPSE
        ///We need to add overlay to be sure all glass filled.

        let ellipseHeight = overlayEllipseHeight * 0.7

        let topOverlayEllipseWidth = currentOverlayXEnd - currentOverlayXStart
        let topOverlayEllipseRect = CGRect(x: currentOverlayXStart, y: currentOverlayYValue - ellipseHeight / 2.0, width: topOverlayEllipseWidth, height: ellipseHeight)
        
        let ellipsePath = UIBezierPath(ovalIn: topOverlayEllipseRect)
        let ellipseLayer = getShapeLayer(for: ellipsePath.cgPath, with: self.fillDarkColor)
        fillCAShapeLayerArrayNew.append(ellipseLayer)
        
        _plateFluidPartView.layer.addSublayer(ellipseLayer)
        
        let fillEllipseAnimation = getFillAnimation(with: CFTimeInterval(animationTime/4), fromColor: self.fillDarkColor.withAlphaComponent(0.6).cgColor, toColor: self.fillDarkColor.cgColor)
        
        ellipseLayer.add(fillEllipseAnimation, forKey: "fillColor")
        
        _previousProgressPercentage = newProgress
    }

}

extension HFFluidPlate {
    
    fileprivate func getShapeLayer(for path: CGPath, with fillColor: UIColor) -> CAShapeLayer {
        let shape = CAShapeLayer()
        shape.path = path
        shape.lineWidth = self.edgeWidth
        shape.fillColor = fillColor.cgColor
        
        return shape
    }
    
    fileprivate func getShapeLayerWithDefaultParameters(for path: CGPath) -> CAShapeLayer {
        let shape = CAShapeLayer()
        shape.path = path
        shape.lineWidth = self.edgeWidth
        shape.fillColor = self.fillLightColor.cgColor
        
        return shape
    }
    
    fileprivate func getFillAnimation(with duration: CFTimeInterval, forColor color: UIColor) -> CABasicAnimation {
        let fillAnimation = CABasicAnimation(keyPath: "fillColor")
        fillAnimation.duration = duration
        fillAnimation.fromValue = UIColor.clear.cgColor
        fillAnimation.toValue = color.cgColor
        
        return fillAnimation
    }
    
    fileprivate func getFillAnimation(with duration: CFTimeInterval,
                                      fromColor: CGColor,
                                      toColor: CGColor) -> CABasicAnimation {
        let fillAnimation = CABasicAnimation(keyPath: "fillColor")
        fillAnimation.duration = duration
        fillAnimation.fromValue = fromColor
        fillAnimation.toValue = toColor
        
        return fillAnimation
    }
}
