//
//  HFGranularPlateBackPart.swift
//  Pods
//
//  Created by Dmitry Antonenka on 4/30/17.
//
//

import UIKit

class HFGranularPlateBackPart: UIView {
    @IBInspectable open var edgeColor: UIColor = UIColor.white {
        didSet {
            setNeedsDisplay()
        }
    }
    @IBInspectable open var edgeWidth: CGFloat = 3.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        backgroundColor = .clear

        PlateStyleKit.drawGranularPlateBackPart(plateFrame: rect, edgeWidth: edgeWidth, edgeColor: self.edgeColor)
    }
}
