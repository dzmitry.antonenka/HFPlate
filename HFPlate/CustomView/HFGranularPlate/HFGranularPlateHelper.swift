//
//  HFGranularPlateHelper.swift
//  HFPlate
//
//  Created by Dmitry Antonenka on 4/1/17.
//  Copyright © 2017 BSUIR. All rights reserved.
//

import UIKit
import GameplayKit

class HFGranularPlateHelper {
    static let shared = HFGranularPlateHelper()

    static var plateFillMultiplier: CGFloat = 1.0
    
    fileprivate var _granuleSizePrivateMultiplier: CGFloat = 0.06
    var granuleSizeMultiplier: CGFloat = 1.0 {
        willSet(newValue) {
           _granuleSizePrivateMultiplier = newValue * 0.06
        }
    }
    
    let plateWidthMarginMultiplier: CGFloat = 0.2

    //Used to define deviation for start items Y position(before they fall to the plate).
    let yDeviationCoefficient: CGFloat = 0.012

    func fastFloor(_ x: CGFloat) -> CGFloat {
        return floor( x )
    }

    func generateGranuleItems(fillCurrentProgress: CGFloat,
                              fillNewProgress: CGFloat,
                              plateSize: CGSize,
                              granuleStrokeWidth: CGFloat,
                              granuleStrokeColor: UIColor,
                              granuleFillColor: UIColor) -> [GranuleView]? {


        let progressDiff = fillNewProgress - fillCurrentProgress

        // In case when we need to display more items then we have, let's generate it
        if progressDiff > 0 {
            let approximateAvailableWidth = plateSize.width - plateSize.width * plateWidthMarginMultiplier
            let approximateAvailableHeight = plateSize.height * progressDiff * HFGranularPlateHelper.plateFillMultiplier

            let granuleSide: CGFloat = approximateAvailableWidth * _granuleSizePrivateMultiplier


            let numberOfItemsToFillOneLevel = fastFloor( approximateAvailableWidth / granuleSide )
            let numberOfLevelsFillLeft = fastFloor( approximateAvailableHeight / granuleSide )

            let itemsAmountRequired: Int = Int( fastFloor( numberOfItemsToFillOneLevel * numberOfLevelsFillLeft ) )

            //If no elements to add, return nil
            guard itemsAmountRequired > 0 else {
                return nil
            }

            let random = GKRandomSource()
            let gaussianDistributionXAxis = GKGaussianDistribution( randomSource: random, lowestValue: Int( granuleSide ), highestValue: Int( plateSize.width - granuleSide ) )

            let yDeviation = Int( plateSize.height * yDeviationCoefficient )
            let gaussianDistributionYAxis = GKGaussianDistribution( randomSource: random, lowestValue: -yDeviation, highestValue: yDeviation )

            let yStartPosition = plateSize.height * 0.5

            var array = [GranuleView]()
            for _ in 1...itemsAmountRequired {

                let itemXPosition = CGFloat( gaussianDistributionXAxis.nextInt() )
                let itemYDeviation = CGFloat( gaussianDistributionYAxis.nextInt() )

                let granuleRect = CGRect( x: itemXPosition, y: yStartPosition + itemYDeviation, width: granuleSide, height: granuleSide )
                
                
                let granuleType: GranuleViewType = .circle
                
                let granuleView = GranuleView(frame: granuleRect, granuleStrokeWidth: granuleStrokeWidth, borderColor: granuleStrokeColor, fillColor: granuleFillColor, type: granuleType)

                granuleView.layer.masksToBounds = true
                array.append( granuleView )
            }

            return array
        }

        return nil
    }
}
