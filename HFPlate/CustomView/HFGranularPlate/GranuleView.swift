//
//  GranuleView.swift
//  HFPlate
//
//  Created by Dmitry Antonenka on 4/1/17.
//  Copyright © 2017 BSUIR. All rights reserved.
//

import UIKit

enum GranuleViewType: Int {
    case unknown
    case star
    case circle
}

open class GranuleView: UIView {
    fileprivate var borderWidth: CGFloat = 3.0
    
    //fill color calculates based on borderColor in `PlateStyleKit.drawGranule` method.
    fileprivate var borderColor: UIColor!
    fileprivate var fillColor: UIColor!
    
    fileprivate var type: GranuleViewType!
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = UIColor.clear
    }

    
    init(frame: CGRect, granuleStrokeWidth: CGFloat = 1.0,
         borderColor: UIColor!, fillColor: UIColor, type: GranuleViewType) {
        super.init(frame: frame)

        backgroundColor = UIColor.clear

        assert(type != .unknown, "GranuleView assert failure: Unknown granule type!")
        
        self.type = type
        self.borderWidth = granuleStrokeWidth
        self.fillColor = fillColor
        self.borderColor = borderColor
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override open func draw(_ rect: CGRect) {
        backgroundColor = UIColor.clear
        self.isOpaque = true
        
        if type == .star {
            PlateStyleKit.drawStarLikeGranule(frame: rect, granuleBorderWidth: borderWidth, granuleStrokeColor: borderColor, granuleFillColor: fillColor)
        } else {
            PlateStyleKit.drawCircleLikeGranule(frame: rect, granuleBorderWidth: borderWidth, granuleStrokeColor: borderColor, granuleFillColor: fillColor)
        }
    }
}


//---------------------------------------------------
//MARK: UIDynamicItemCollision overriders

extension GranuleView {
    override open var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        return .ellipse
    }
    
    override open var collisionBoundingPath: UIBezierPath {
        if type == .star {
            return PlateStyleKit.generateStarLikeGranuleBezierPath(frame: self.frame)
        } else {
            return PlateStyleKit.generateCircleGranuleBezierPath(frame: self.frame, granulBorderWidth: borderWidth)
        }
    }
}
