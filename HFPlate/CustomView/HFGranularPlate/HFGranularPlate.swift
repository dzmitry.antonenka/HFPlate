//
//  HFGranularPlate.swift
//  HFGranularPlate
//
//  Created by Dmitry Antonenka on 4/1/17.
//  Copyright © 2017 BSUIR. All rights reserved.
//

import UIKit

@IBDesignable
open class HFGranularPlate: HFPlate {

    //--------------------------------------------------------
    //MARK: @IBInspectable properties

    @IBInspectable open var granuleSizeMultuplier: CGFloat = 1.0 {
        didSet {
            HFGranularPlateHelper.shared.granuleSizeMultiplier = granuleSizeMultuplier
            
            setNeedsDisplay()
        }
    }
    
    @IBInspectable open var granuleFillMultuplier: CGFloat = 0.7 {
        didSet {
            HFGranularPlateHelper.plateFillMultiplier = granuleFillMultuplier
            
            setNeedsDisplay()
        }
    }
    
    @IBInspectable open var granuleBorderColor: UIColor = UIColor.brown {
        didSet {
            setNeedsDisplay()
        }
    }

    @IBInspectable open var granuleFillColor: UIColor = UIColor.yellow {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable open var granuleBorderWidth: CGFloat = 1.0 {
        didSet {
            setNeedsDisplay()
        }
    }

    //--------------------------------------------------------
    //MARK: Fileprivate properties

    fileprivate var _dynamicAnimator: UIDynamicAnimator!
    fileprivate var _gravityBehaviour: UIGravityBehavior!
    fileprivate var _collision: UICollisionBehavior!

    //Array of granules to display display collision
    fileprivate var _itemsHolderView: UIView!

    fileprivate var viewItems: [GranuleView]! = [GranuleView]()

    
    fileprivate var _frontPlatePart: HFGranularPlateFrontPart!
    fileprivate var _backPlatePart: HFGranularPlateBackPart!
    
    //============================================================
    //MARK: Interface methods
    
    override open func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()

        UIGraphicsBeginImageContext(self.frame.size)
        
        backgroundColor = UIColor.clear
        initialize()
        
        _isLoaded = true
        draw(self.frame)
        
        UIGraphicsEndImageContext();
    }

    override open func awakeFromNib() {
        super.awakeFromNib()

        initialize()
        
        _isLoaded = true
    }
    
    func initialize() {
        allocAndBindPlateBackPartView()
        allocAndBindItemHolderView()
        allocAndBindPlateFrontPartView()
        
        initDynamicHelpers()
    }

    fileprivate func allocAndBindPlateBackPartView() {
        _backPlatePart = HFGranularPlateBackPart()
        _backPlatePart.edgeWidth = self.edgeWidth
        _backPlatePart.edgeColor = self.edgeColor
        _backPlatePart.backgroundColor = .clear

        _backPlatePart.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(_backPlatePart)
        
        let views = ["backPlatePart": _backPlatePart as Any] as [String : Any]
        var allConstraints = [NSLayoutConstraint]()
        
        let horizontalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[backPlatePart]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += horizontalConstraints
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[backPlatePart]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        NSLayoutConstraint.activate(allConstraints)
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    fileprivate func allocAndBindPlateFrontPartView() {
        _frontPlatePart = HFGranularPlateFrontPart()
        _frontPlatePart.edgeWidth = self.edgeWidth
        _frontPlatePart.edgeColor = self.edgeColor
        _frontPlatePart.backgroundColor = .clear

        _frontPlatePart.translatesAutoresizingMaskIntoConstraints = false
        
        self.insertSubview(_frontPlatePart, aboveSubview: _itemsHolderView)
        
        let views = ["frontPlatePart": _frontPlatePart as Any] as [String : Any]
        var allConstraints = [NSLayoutConstraint]()
        
        let horizontalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[frontPlatePart]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += horizontalConstraints
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[frontPlatePart]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        NSLayoutConstraint.activate(allConstraints)
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    fileprivate func allocAndBindItemHolderView() {
        _itemsHolderView = UIView(frame: self.bounds)
        _itemsHolderView.backgroundColor = UIColor.clear
        _itemsHolderView.translatesAutoresizingMaskIntoConstraints = false
        _itemsHolderView.accessibilityIdentifier = "_itemsHolderView"
        
        self.insertSubview(_itemsHolderView, aboveSubview: _backPlatePart)
        
        //Bind anchors
        _itemsHolderView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        _itemsHolderView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        _itemsHolderView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        _itemsHolderView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        _itemsHolderView.setNeedsLayout()
        _itemsHolderView.layoutIfNeeded()
    }

    
    override open func draw(_ rect: CGRect) {
        backgroundColor = UIColor.clear
        
        _backPlatePart.setNeedsDisplay(rect)
        _itemsHolderView.setNeedsDisplay(rect)
        _frontPlatePart.setNeedsDisplay(rect)
        
        //This methods re-assign boundary collision every redraw, because its could be different
        assignBoundaryForCollisionBehavior()
    }

    //============================================================
    //MARK: Fileprivate methods

    override func animateFill(from oldProgress: CGFloat, to newProgress: CGFloat) {
        assert( newProgress >= 0 && newProgress <= 1, "FillProgress should be in range 0...1. Current value \(newProgress)")
        let progressDiff = newProgress - _previousProgressPercentage

        guard progressDiff != 0 else { return }


        if progressDiff > 0 { //Need to generate items if not enough
            if let newItems = HFGranularPlateHelper.shared.generateGranuleItems(fillCurrentProgress:    oldProgress,
                    fillNewProgress: newProgress, plateSize: self.bounds.size,
                    granuleStrokeWidth: granuleBorderWidth,
                    granuleStrokeColor: granuleBorderColor,
                    granuleFillColor: granuleFillColor) {
                
                addProportionallyNewItems(newItems)
            }
        } else {
            
            //Or remove if there is too much.
            // ----------- Just simple proportion ------------------
            //            oldProgress -- viewItems.count
            //            fillProgress --       @{X}
            //            @{X} = (fillProgress * viewItems.count) / oldProgress

            var itemsCountToRemove: Int = 0
            itemsCountToRemove = viewItems.count - Int( newProgress * CGFloat(viewItems.count) / oldProgress)

            let startIndex = viewItems.count - 1
            let endIndex = startIndex - itemsCountToRemove

            for (index, _) in viewItems.enumerated().reversed() {
                if index < endIndex { break }
                let viewItem = viewItems[index]

                UIView.animate(withDuration: 0.33, delay: 0.0, options: [.allowUserInteraction], animations: {
                    self._gravityBehaviour.removeItem(viewItem)
                    self._collision.removeItem(viewItem)
                    viewItem.removeFromSuperview()
                }, completion: { (completed) in
                    self.viewItems.remove(at: index)
                })
            }
        }

        _previousProgressPercentage = _currentProgressPercentage

        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.setNeedsDisplay()
        }
    }

    fileprivate func initDynamicHelpers() {
        _dynamicAnimator = UIDynamicAnimator(referenceView: self._itemsHolderView)

        _gravityBehaviour = UIGravityBehavior()
        _gravityBehaviour.magnitude = 0.25

        _dynamicAnimator.addBehavior(_gravityBehaviour)

        _collision = UICollisionBehavior(items: viewItems)

//Let it fall
//_collision.translatesReferenceBoundsIntoBoundary = true

        _dynamicAnimator.addBehavior(_collision)
    }

    fileprivate func assignBoundaryForCollisionBehavior() {
        let boundaryIdentifier = "plateBottomCurvedBorder"
        let bezierPath = PlateStyleKit.generateGranularPlateBottomCurveBezierPath(plateFrame: self.bounds, edgeWidth: 2.0)

//Help snippet to render border that used in collision behaviour. For TEST only
//  UIColor.red.setStroke()
//  bezierPath.stroke()

        _collision.removeBoundary(withIdentifier: boundaryIdentifier as NSCopying)
        _collision.addBoundary(withIdentifier: boundaryIdentifier as NSCopying, for: bezierPath)
    }

    fileprivate func addProportionallyNewItems(_ newItems: [GranuleView]) {
        for item in newItems {
            self._itemsHolderView.addSubview(item)
            self.viewItems.append(item)

            self._gravityBehaviour.addItem(item)
            self._collision.addItem(item)
        }
    }
}

extension HFGranularPlate: UICollisionBehaviorDelegate {
    override open var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        return .ellipse
    }

    override open var collisionBoundingPath: UIBezierPath {
        return PlateStyleKit.generateGranularPlateBottomCurveBezierPath(plateFrame: self.bounds, edgeWidth: 2.0)
    }
}
