Pod::Spec.new do |s|

  s.name         = "HFPlate"
  s.version      = "1.0.3"
  s.summary      = "HFPlate - custom plate view, that created specially for HealthAndFood iOS project."

  s.description  = "HFPlate - custom plate view, that created specially for HealthAndFood iOS project. Created to render volume of consumed dish. Plate support dish of 2 types: fluid & granular."

  s.homepage     = "http://healthandfood.azurewebsites.net"
  s.license = { :type => 'MIT', :file => 'MIT-LICENSE.txt' }

  s.author             = "schurik77799@gmail.com"

  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://dzmitry.antonenka@gitlab.com/dzmitry.antonenka/HFPlate.git", :tag => "1.0.3" }

  s.source_files  = 'HFPlate/**/*.{swift}'
end
